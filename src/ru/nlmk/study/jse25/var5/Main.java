package ru.nlmk.study.jse25.var5;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Point p1 = new Point(1.0, 2.0);
        Point p2 = new Point(4.0, 2.0);
        Point p3 = new Point(3.0, 3.0);

        Line line1 = new Line(p1, p2, 1);
        Line line2 = new Line(p2, p3, 2);
        String fileName = "pointsFile.data";

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(fileName))){
            objectOutputStream.writeObject(line1);
            objectOutputStream.writeObject(line2);
            line1.setPoint1(new Point(2.0, 13.0));
            objectOutputStream.reset();
            objectOutputStream.writeObject(line1);
            objectOutputStream.writeObject(p1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<Line> lines = new ArrayList<>();
        Point p4 = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileName))){
            for (int i = 0; i < 3; i++){
                lines.add((Line) objectInputStream.readObject());
            }
            p4 = (Point) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(lines);
        System.out.println(p4);
    }
}
