package ru.nlmk.study.jse25.var4;

import java.io.*;

public class Main4 {
    public static void main(String[] args) {
        Person tom = new Person("Tom", 35, 1.95, true);
        try (DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream("data.bin"))){
            dataOutputStream.writeUTF(tom.getName());
            dataOutputStream.writeInt(tom.getAge());
            dataOutputStream.writeDouble(tom.getHeight());
            dataOutputStream.writeBoolean(tom.isMarried());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try(DataInputStream dataInputStream = new DataInputStream(new FileInputStream("data.bin"))){
            Person tomClone = new Person();
            tomClone.setName(dataInputStream.readUTF());
            tomClone.setAge(dataInputStream.readInt());
            tomClone.setHeight(dataInputStream.readDouble());
            tomClone.setMarried(dataInputStream.readBoolean());
            System.out.println(tomClone);
            System.out.println(tom);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
