package ru.nlmk.study.jse25.var7;

import ru.nlmk.study.jse25.var7.model.Person;

import java.util.Arrays;

public class Main {
    private static final String FILE_NAME = "saveddata.sav";

    public static void main(String[] args) {
        Person tom = new Person("Tom", 35, 1.95, true, Arrays.asList(1, 3, 5));

        Serializer serializer = new Serializer();
        serializer.serialize(tom, FILE_NAME);

        Person tomClone = (Person) serializer.deserialize(FILE_NAME, Person.class);
        System.out.println(tomClone);
    }
}
