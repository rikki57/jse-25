package ru.nlmk.study.jse25.var7.model;

import java.util.List;

public class Person {
    private String name;
    private int age;
    private double height;
    private boolean married;
    private List<Integer> numbers;

    public Person() {
    }

    public Person(String name, int age, double height, boolean married, List<Integer> numbers) {
        this.name = name;
        this.age = age;
        this.height = height;
        this.married = married;
        this.numbers = numbers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public List<Integer> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<Integer> numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                ", married=" + married +
                ", numbers=" + numbers +
                '}';
    }
}
