package ru.nlmk.study.jse25.var7;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class DataHandler {
    public Map<String, FieldData> getData(String fileName){
        try(ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream(new File(fileName)))) {
            return (Map<String, FieldData>)objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public boolean saveData(Map<String, FieldData> data, String fileName){
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream(new File(fileName)))){
            objectOutputStream.writeObject(data);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
