package ru.nlmk.study.jse25.var7;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Serializer {
    private final DataHandler dataHandler = new DataHandler();

    public boolean serialize(Object object, String fileName){
        Map<String, FieldData> result = new HashMap<>();
        Class objectClass = object.getClass();
        for (Field field : objectClass.getDeclaredFields()){
            field.setAccessible(true);
            try {
                FieldData fieldData = new FieldData(field.getName(), objectClass.getName(), field.get(object));
                result.put(objectClass.getName() + "." + field.getName(), fieldData);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return dataHandler.saveData(result, fileName);
    }

    public Object deserialize(String fileName, Class clazz){
        Map<String, FieldData> objectData = dataHandler.getData(fileName);
        if (objectData != null) {
            try {
                Object object = clazz.newInstance();
                for (Field field : clazz.getDeclaredFields()) {
                    field.setAccessible(true);
                    field.set(object, objectData.get(clazz.getName() + "." + field.getName()).getFieldValue());
                }
                return object;
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
