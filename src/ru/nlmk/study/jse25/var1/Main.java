package ru.nlmk.study.jse25.var1;

import java.io.ByteArrayInputStream;

public class Main {
    public static void main(String[] args) {
        byte[] array1 = new byte[]{1, 2, 5, 7, 9};
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(array1, 2, 2);
        int b;
/*        while ((b = byteArrayInputStream.read()) != -1){
            System.out.println(b);
        }*/

        String text = "Hello, world!";
        array1 = text.getBytes();
        byteArrayInputStream = new ByteArrayInputStream(array1);
        while ((b = byteArrayInputStream.read()) != -1){
            System.out.println((char) b);
        }
    }
}
