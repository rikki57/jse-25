package ru.nlmk.study.jse25.var2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        writeFile("note.txt", "Java is a rather complicated programming language");
        readFile("note.txt");
    }

    public static void readFile(String fileName){
        try (FileInputStream fileInputStream = new FileInputStream(fileName)){
            System.out.println("File size: " + fileInputStream.available());
            int i = -1;
            while ((i = fileInputStream.read()) != -1){
                System.out.println((char) i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeFile(String fileName, String content){
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName)){
            byte[] buffer = content.getBytes();
            fileOutputStream.write(buffer, 4, 10);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
