package ru.nlmk.study.jse25.var3;

import java.io.*;

public class Main {
    public static void main(String[] args) {
        String textToWrite = "Hello world buffered";
        //writeFile("note.txt", textToWrite);
        //readFile("note.txt");
        readArray();

    }

    private static void writeFile(String fileName, String textToWrite){
        try (BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(
                new FileOutputStream(fileName))){
            byte[] bytes = textToWrite.getBytes();
            bufferedOutputStream.write(bytes);
            bufferedOutputStream.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readArray(){
        String text = "Some kind of long string string is very long";
        byte[] byteText = text.getBytes();
        try(BufferedInputStream bufferedInputStream = new BufferedInputStream(new ByteArrayInputStream(byteText))) {
            int c;
            while ((c = bufferedInputStream.read()) != -1){
                System.out.println((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static  void readFile(String fileName){
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null){
                System.out.println(currentLine);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
